from flask_restful import Resource, reqparse
from models import UserModel, RevokedTokenModel
#from flask_jwt_extended import (create_access_token, create_refresh_token, jwt_required, get_jwt_identity, jwt_refresh_token_required, get_raw_jwt)
from flask_jwt_extended import (
    create_access_token,
    create_refresh_token,
    jwt_required,
    get_jwt,
    get_jwt_identity
)

parser = reqparse.RequestParser()
parser.add_argument('firstname')
parser.add_argument('lastname')
parser.add_argument('email', help = 'This field cannot be blank', required=True)
parser.add_argument('password', help = 'This field cannot be blank', required=True)

class UserRegistration(Resource):
    def post(self):
        data = parser.parse_args()
        if(UserModel.find_by_email(data['email'])):
            return {'message': 'User {} was already exists'.format(data['email'])}
        new_user = UserModel(
            firstname = data['firstname'],
            lastname = data['lastname'],
            email = data['email'],
            password = UserModel.generate_hash(data['password'])
        )
        try:
            new_user.save_to_db()
            access_token = create_access_token(identify = data['email'])
            refresh_token = create_refresh_token(identify = data['email'])
            return {
                'message': 'User {} was created'.format( data['email'] ),
                'access_token' : access_token,
                'refresh_token' : refresh_token
            }
        except:
            return {'message': 'Something went wrong'}, 500
    
class UserLogin(Resource):
    def post(self):
        data = parser.parse_args()
        current_user = UserModel.find_by_email(data['email'])
        if not current_user:
            return {'message': 'User {} doesn\'t exist'.format(data['email'])}
        
        if UserModel.verify_hash(data['password'],current_user.password):
            access_token = create_access_token(data['email'])
            refresh_token = create_refresh_token(data['email'])
            return {
                'message': 'Logged in as {}'.format(data['email']),
                'access_token' : access_token,
                'refresh_token' : refresh_token
                }
        else:
            return {'message': 'Wrong Credentials'}
        
        #return {'message': 'User Login'}
    
class UserLogoutAccess(Resource):
    @jwt_required()
    def post(self):
        jti = get_jwt()['jti']
        try:
            revoked_token = RevokedTokenModel(jti = jti)
            revoked_token.add()
            return {'message':'Access token has been revoked'}
        except:
            return {'message':'Something went wrong'}, 500

class UserLogoutRefresh(Resource):
    @jwt_required(refresh=True)
    def post(self):
        jti = get_jwt()['jti']
        try:
            revoked_token = RevokedTokenModel(jti = jti)
            revoked_token.add()
            return {'message': 'token revoked'}
        except:
            return {'message': 'Something went wrong'}, 500
    
class TokenRefresh(Resource):
    @jwt_required(refresh=True)
    def post(self):
        current_user = get_jwt_identity()
        access_token = create_access_token(identity = current_user)
        return {'access_token': access_token}
    
class AllUsers(Resource):
    @jwt_required()
    def get(self):
        return UserModel.return_all()
    
    def delete(self):
        return UserModel.delete_all()
    
class SecretResource(Resource):
    @jwt_required()
    def get(self):
        return {'response':147}